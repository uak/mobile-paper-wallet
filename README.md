# Mobile Paper Wallet

A paper wallet like mobile application which allows you to create a single use wallet on your phone. Programmed using Python and Kivy.

## Idea
Imagine creating a token wich allows a person to go to a merchant and exchange it for bread or food, 1 bread token = 1 loaf of bread. Upon reciving the token the user will go to the bakery and expose the wif key qrcode. The bakery will scan it, withdraw fund and handle the user a loaf of bread.

User doesn't need to have an active internet connection on their mobile.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.gitlab.uak.mobile_paper_wallet/)

## ScreenShots
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.jpg"  width="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.jpg"  width="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.jpg"  width="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.jpg"  width="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/5.jpg"  width="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/6.jpg"  width="250">

### Generating New Keys
As generated walletsare single use wallet, you can generate another one from the Keys screen. 

#### Generating a Wallet
To create a new wallet, user is asked to enter a confirmation word so they don't mistakenly hide the older wallet. Wallets could be also imported using a WIF.

## Supported coins


 * Bitcoin
 * Bitcoin Cash
 * Ergon
 * Nexa
 * Simple Ledger Protocol (simpleledger)

 ## Usage

 To use the app you can send coins of supported types to the address on the address screen either by scanning the QR code or by clicking on the screen and copying the address. 

 To withdraw tokens you use a wallet that supports **sweep** functionality. Electrum for BTC, Electron Cash for BCH.

## License

GPL v3 or later
