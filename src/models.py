from peewee import *
from peewee import IntegrityError
import datetime
import json

database_file = "db.sqlite3"

db = SqliteDatabase(database_file)


class Settings(Model):
    """The database class"""
    _id = AutoField(unique=True)
    balance_api = CharField(max_length=60, null=True)
    token_id = CharField(max_length=60, null=True)
    coin_prefix = CharField(max_length=10, null=True)

    class Meta:
        database = db  # This model uses the "db" database.


Settings.create_table()


class Wallets(Model):
    """The database class"""
    _id = AutoField(unique=True)
    address = CharField(max_length=60)
    wif = CharField(max_length=60)
    date = CharField(max_length=15)

    class Meta:
        database = db  # This model uses the "db" database.


Wallets.create_table()


def db_save_settings(balance_api=None, token_id=None, coin_prefix=None):
    """
    Save settings to database
    """
    db.connect(reuse_if_open=True)
    try:
        Settings.create(
            _id=1,
            balance_api=balance_api,
            token_id=token_id,
            coin_prefix=coin_prefix
        )
        db.close()
        return True
    except IntegrityError:
        db.close()
        return False


def db_save_wallets(address, wif):
    """
    Save settings to database
    """
    db.connect(reuse_if_open=True)
    try:
        Wallets.create(
            address=address,
            wif=wif,
            date=datetime.datetime.now()
        )
        db.close()
        return True
    except IntegrityError:
        db.close()
        return False


def export_keys():
    """
    Export wallet keys from database to json format
    """
    db.connect(reuse_if_open=True)
    try:
        cursor = db.cursor()
        cursor.execute('''SELECT * from wallets ''')
        result = cursor.fetchall()
        return json.dumps(result, sort_keys=True, indent=4)
    except IntegrityError:
        db.close()
        raise IntegrityError
