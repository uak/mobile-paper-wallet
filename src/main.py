#!/usr/bin/env python3

"""
    main.py: Main file for the Needy Mobile App.
    The Needy Mobile App uses Kivy framework.
"""

# Kivy imports
from kivy.core.window import Window
from kivy.core.clipboard import Clipboard
from kivy.app import App
from kivy.lang import Builder
from kivy.lang import Observable
from kivy.logger import Logger, LOG_LEVELS

from kivy.properties import (
                            ObjectProperty,
                            StringProperty,
                            BooleanProperty,
                            NumericProperty,
                            ListProperty
                            )

# Kivy uix imports
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.checkbox import CheckBox
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.popup import Popup
from kivy.uix.progressbar import ProgressBar
from kivy.utils import platform

# Python imports
from functools import partial
from os.path import dirname, join
import threading
import requests
import datetime
import gettext  # multilanguage support
import sqlite3  # database to store data
import time
import os

# External libraries imports
from bitcoinaddress import Wallet
from ecashaddress import convert
from ecashaddress.convert import Address

# Importing peewee database module
from models import (
                    Settings,
                    Wallets,
                    db_save_wallets,
                    db_save_settings,
                    export_keys
                    )

from app_functions import (
    generate_qr_code_function,
    get_wif_address,
    convert_comp_wif_to_uncomp,
)

__author__ = "uak@gitlab"

__version__ = "1.4.1"

KIVY_LOG_LEVEL = os.environ.get('KIVY_LOG_LEVEL', "info")
Logger.setLevel(LOG_LEVELS[KIVY_LOG_LEVEL])

home_dir = os.path.expanduser("~")

# Variables
app_name = 'mobile_paper_wallet'
database_file = "sqlite3.db"
app_icon_file = "data/icon.png"
address_qrcode_file = "media/address.png"
wif_qrcode_file = "media/wif.png"
media_dir = "media/"
layout_file = 'layout.kv'
wallets_export_file = "wallets.json"
default_coin_prefix = "bitcoincash"
help_link = "https://gitlab.com/uak/mobile-paper-wallet"
custom_font = "data/Amiri-Regular.ttf"  # works with Arabic & English letters
wif_length = 51
compressed_wif_length = 52


# Password to allow deleting keys
settings = {
    "confirm_delete_text": "delete",
    "app_language": "en",
}

# List of supported coins with their testnet state
supported_coins = {
    "bitcoincash": {"testnet": False},
    "bchtest": {"testnet": True},
    "simpleledger": {"testnet": False},
    "ergon": {"testnet": False},
    "nexa": {"testnet": False},
    "bitcoin": {"testnet": False},
    "bitcoin_test": {"testnet": True},
    "slptest": {"testnet": True},
}


class MyManager(ScreenManager):
    """The screen manager that handles app screens
    """
    pass


class MainBox(BoxLayout):
    """Mainbox under MainApp
    It contains the ScreenManager
    """
    pass


# gettext support based on https://github.com/tito/kivy-gettext-example
class Lang(Observable):
    """
    Support gettext in Kivy based on `tito` example.
    """
    observers = []
    lang = None

    def __init__(self, defaultlang):
        super(Lang, self).__init__()
        self.ugettext = None
        self.lang = defaultlang
        self.switch_lang(self.lang)

    def _(self, text):
        return self.ugettext(text)

    def fbind(self, name, func, args, **kwargs):
        if name == "_":
            self.observers.append((func, args, kwargs))
        else:
            return super(Lang, self).fbind(name, func, *args, **kwargs)

    def funbind(self, name, func, args, **kwargs):
        if name == "_":
            key = (func, args, kwargs)
            if key in self.observers:
                self.observers.remove(key)
        else:
            return super(Lang, self).funbind(name, func, *args, **kwargs)

    def switch_lang(self, lang):
        # get the right locales directory, and instanciate a gettext
        locale_dir = join(dirname(__file__), 'data', 'locales')
        locales = gettext.translation(app_name, locale_dir, languages=[lang])
        self.ugettext = locales.gettext
        self.lang = lang

        # update all the kv rules attached to this text
        for func, largs, kwargs in self.observers:
            func(largs, None, None)


# Set the language of App
tr = Lang(settings["app_language"])


# Long text
setting_change_warning_txt = tr._(
    """
Please be careful!\nChange settings with caution.
"""
)
wallet_regenerate_warning_txt = tr._(
    """
Changes saved!\nPlease regenerate the wallet to start using the new prefix
"""
)


class SaveDialog(FloatLayout):
    """The file save dialog class
    """
    save_to_storage = ObjectProperty(None)
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)


# Used with ScreenManager
class QrCodeScreen(Screen):
    pass


class SettingsScreen(Screen):
    """
    Screen to edit settings
    """
    pass


class InfoPopup(RelativeLayout):
    pass


class ConfirmPopup(RelativeLayout):
    pass


class ImportWIFPopup(RelativeLayout):
    pass


class ChooseCoinPopup(RelativeLayout):
    pass


class MainApp(App):
    """
    Main App class
    """
    lang = StringProperty(settings["app_language"])
    coin_prefix = StringProperty()
    prefix_changed_recently = BooleanProperty()

    def on_lang(self, instance, lang):
        tr.switch_lang(lang)

    def ar_shape(self, text):
        """
        Convert Arabic text for non compatible display
        """
        reshaped_text = arabic_reshaper.reshape(text)
        return get_display(reshaped_text)

    def save_settings(self):
        """
        Save settings from configration screen to the database.
        """
        self.update_settings = Settings.update(
            balance_api="",
            token_id="",
            coin_prefix=self.root.ids.settings_screen.ids.coin_prefix.text,
            ).where(Settings._id == 1)
        self.update_settings.execute()
        if self.prefix_match() is False:
            self.prefix_changed_recently = True
            error_text = wallet_regenerate_warning_txt
            self.show_info_popup(error_text)
            Logger.debug("save_settings: Data saved. Prefix mismatch.")
        else:
            Logger.debug("save_settings: Data saved. Prefix not altered.")

    def create_wallet(self, prefix, testnet=False, _wif=None):
        """Create wallet using bitcoinaddress lib then convert it to
        SLP address using cashaddress lib
        """
        # self.app_settings = Settings.get(Settings._id == 1)
        # Create wallet address using ecashaddress lib and set custom prefix
        Logger.debug(f"create_wallet: in wallet generation {prefix}")
        try:
            wallet_dict = get_wif_address(prefix, testnet, _wif)
            self.address = wallet_dict["address"]
            self.wif = wallet_dict["wif"]
            db_save_wallets(self.address,  self.wif)
            Logger.info(f"create_wallet: new wallet address {self.address} .")
            Logger.debug(f"create_wallet: new wallet WIF {self.wif} .")
            self.prefix_changed_recently = False
        except Exception as e:
            error_text = "There is an issue with generating wallet"
            self.show_info_popup(error_text)
            Logger.debug(f"create_wallet: Exception {e}")

    def generate_qr_code(self):
        """Generate QR code for address and WIF
        """
        generate_qr_code_function(
            self.address,
            media_dir,
            address_qrcode_file
        )
        generate_qr_code_function(
            self.wif,
            media_dir,
            wif_qrcode_file
        )
        # Logging the operation to loglevel debug
        Logger.debug(
            f"generate_qr_code: Qrcode file {self.address} and WIF created. "
        )

    def prefix_match(self):
        """Check if database record of current address matches settings
        address prefix.
        """
        self.app_settings = Settings.get(Settings._id == 1)
        _coin_prefix = self.app_settings.coin_prefix
        wallet_records = Wallets.select().order_by(Wallets._id.desc()).get()
        _address = wallet_records.address
        record_prefix = _address.split(":")[0]
        Logger.debug(f"prefix_match: Record_prefix {record_prefix}")
        Logger.debug(
            f"prefix_match: app settings {_coin_prefix}"
        )

        if (
            _coin_prefix not in ("bitcoin", "bitcoin_test")
            and record_prefix == _coin_prefix
        ):
            Logger.debug(
                'prefix_match: Not Bitcoin match.'
            )
            return True
        # Check if address is not cashaddress and it's bitcoin address with bc
        elif (
            ":" not in _address
            and _address.startswith("bc")
            and (_coin_prefix == "bitcoin")
        ):
            Logger.debug(
                'prefix_match: wallet starts with bc'
            )
            return True
        elif (
            ":" not in _address
            and _address.startswith("tb")
            and (_coin_prefix == "bitcoin_test")
        ):
            Logger.debug(
                'prefix_match: wallet starts with tb'
            )
            return True
        else:
            Logger.debug(
                f"prefix_match: No match: prefix:{_coin_prefix}"
            )
            return False

    def view_address(self):
        """Address QRcode screen
        """
        # Set source of address qrcode image
        try:
            self.root.qrcode_img.source = address_qrcode_file
            Logger.debug(f"view_address: {address_qrcode_file} loaded")
        except Exception as e:
            Logger.info(f"Unable to load Qrcode file")
            raise e
        Logger.info(f"Address shown: {self.address}")
        # Force reload of image to fix issue not reading file
        self.root.qrcode_img.reload()
        # Wait for some time before continuing
        # time.sleep(0.5)
        # Assign labels
        if self.prefix_match():
            self.root.ids.sm.qrcode_label.text = self.address
            self.root.ids.sm.qrcode_screen_label.text = "Address"
        else:
            self.root.ids.sm.qrcode_label.text = tr._("Regenerate wallet!")
            Logger.info(f"view_address: Prefix mismatch")
        # Switch to screen0
        self.root.ids.sm.current = 'QrCodeScreen'

    def copy_label(self, dt):
        Clipboard.copy(self.root.ids.sm.qrcode_label.text)
        self.root.ids.qr_code_screen.ids.copy_message.text = tr._("Copied!")
        Logger.debug("wif copied to clipboard")

    def view_wif(self):
        """WIF QRcode screen
        """
        # Set source of WIF QRcode image
        self.root.qrcode_img.source = wif_qrcode_file
        # Force reload of image to fix issue not reading file
        self.root.qrcode_img.reload()
        # Wait for some time before continuing
        time.sleep(0.5)
        # Assign labels
        Logger.debug(f"Wif shown {self.address}")
        self.root.ids.sm.qrcode_screen_label.text = "WIF"
        self.root.ids.sm.qrcode_label.text = self.wif
        # Switch to screen
        self.root.ids.sm.current = 'QrCodeScreen'

    def show_settings(self):
        """Move to settings screen
        """
        error_text = setting_change_warning_txt
        self.show_info_popup(error_text)
        self.app_settings = Settings.get(Settings._id == 1)
        wallet_records = Wallets.select().order_by(Wallets._id.desc()).get()
        self.root.ids.sm.current_record.text = (
            str(wallet_records._id) +
            ", " +
            wallet_records.address
        )
        setting_screen_ids = self.root.ids.settings_screen.ids
        setting_screen_ids.coin_prefix.text = self.app_settings.coin_prefix
        self.root.ids.sm.current = 'SettingsScreen'

    def first_start(self):
        """
        Checks to be done at first start of the App
        """
        try:
            # Create setting database if not exist
            if Settings.select().exists():
                self.app_settings = Settings.get(Settings._id == 1)
                Logger.debug(f"first_start: Got existing settings.")
            else:
                db_save_settings(
                                balance_api="",
                                token_id="",
                                coin_prefix=default_coin_prefix,
                                )
                self.app_settings = Settings.get(Settings._id == 1)
                Logger.debug(f"first_start: Created settings from defaults.")

            # If no entry in database generate wallet and create QRcode
            if Wallets.select().exists() is False:
                self.app_settings = Settings.get(Settings._id == 1)
                self.coin_prefix = self.app_settings.coin_prefix
                self.create_wallet(
                    self.coin_prefix,
                    supported_coins[self.coin_prefix]["testnet"]
                )

                self.generate_qr_code()
                Logger.info(
                    f"no entry was found so new wallet and QR images were created."
                    )
            else:
                # set the wallet address and wif
                wallet = Wallets.select().order_by(Wallets._id.desc()).get()
                self.address = wallet.address
                self.wif = wallet.wif

                # Log it
                Logger.debug(f"first_start: Values used from an existing wallet.")
                Logger.info(f"first_start: Address {self.address}.")
                Logger.debug(f"first_start: WIF {self.wif}.")
            self.view_address()
        except Exception as e:
            raise e

    def show_confirm_popup(self):
        """
        Shows confirmation popup before generating a new wallet and
        replacing the QRcodes with new ones.
        """
        # Create a new instance of the ConfirmPopup class
        self.confirm_popup = ConfirmPopup()

        # Setup the popup window
        popupWindow = Popup(title=tr._("Confirm Generating"),
                            content=self.confirm_popup,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open()  # show the popup

    def wif_import_button(self, wif):
        self.coin_prefix = self.root.ids.settings_screen.ids.coin_prefix.text
        self.wif_import_function(
            self.coin_prefix,
            supported_coins[self.coin_prefix]["testnet"],
            wif
        )

    def wif_import_function(self, prefix, testnet, _wif):
        """
        Function to generate wallet from WIF
        """
        def import_wif(_wif):
            self.create_wallet(prefix, testnet, _wif)
            self.generate_qr_code()
            self.wif_import_popup.popup_text.text = tr._("Done!")
            self.wif_import_popup.import_button.disabled = True
            self.wif_import_popup.popup_text.text = tr._("New wallet generated!")
            self.root.ids.sm.current_record.text = tr._("Recently Changed!")
            Logger.debug("New wallet generated from WIF!")

        if len(_wif) == wif_length:
            import_wif(_wif)
        elif len(_wif) == compressed_wif_length:
            print(f"{_wif=}")
            _wif = convert_comp_wif_to_uncomp(_wif)
            print(f"{_wif=}")
            import_wif(_wif)
            Logger.debug("Compressed WIF converted to non compressed")

        else:
            self.wif_import_popup.popup_text.text = tr._("Invalid WIF!")
            self.wif_import_popup.popup_text.halign = 'center'
            Logger.debug("Invalid WIF!")

    def show_import_popup(self):
        """
        Shows confirmation popup before generating a new wallet and
        replacing the QRcodes with new ones.
        """
        # Create a new instance of the ConfirmPopup class
        self.wif_import_popup = ImportWIFPopup()

        # Setup the popup window
        popupWindow = Popup(title=tr._("Confirm Generating"),
                            content=self.wif_import_popup,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open()  # show the popup

    def show_info_popup(self, error_text):
        """
        Shows info popup
        """
        # Create a new instance of the ConfirmPopup class
        show = InfoPopup()

        # Setup the popup window
        show.ids.popup_label.text = error_text
        popupWindow = Popup(title=tr._("Info"),
                            content=show,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open()  # show the popup

    def show_choose_coin_popup(self):
        """
        Shows choose coin popup
        """
        # Create a new instance of the ConfirmPopup class
        show = ChooseCoinPopup()
        box = StackLayout(orientation="tb-lr")
        for i in supported_coins:
            toggle_button = ToggleButton(text=str(i), group="coins")
            # Using `partial` magic
            toggle_button.bind(
                on_release=partial(self.select_coin, toggle_button, i)
            )
            box.add_widget(toggle_button)
        show.add_widget(box)
        # Setup the popup window
        self.popupWindow = Popup(
                            title=tr._("Choose a Coin"),
                            content=show,
                            size_hint=(.9, .5),
                            auto_dismiss=True
                            )

        # Create the popup window
        self.popupWindow.open()  # show the popup

    def select_coin(self, toggle_button, choice, btn):
        self.root.ids.settings_screen.ids.coin_prefix.text = str(choice)
        self.save_settings()
        self.popupWindow.dismiss()

    def dismiss_popup(self):
        self._popup.dismiss()

    # section for file chooser
    def show_export_window(self):
        """
        Show the export to file dialog
        """
        # Request permission
        try:
            if platform == 'android':
                from android.permissions import request_permissions, Permission
                request_permissions([
                    Permission.READ_EXTERNAL_STORAGE,
                    Permission.WRITE_EXTERNAL_STORAGE
                    ])
                Logger.info('show_export_window: Platform Android.')
            else:
                Logger.info('show_export_window: Platform not Android.')
            content = SaveDialog(
                save_to_storage=self.save_to_storage, cancel=self.dismiss_popup
            )
            self._popup = Popup(title=tr._("Save file"), content=content,
                                size_hint=(0.9, 0.9))
            self._popup.open()
        except Exception as e:
            error_text = "Unable to export keys."
            self.show_info_popup(error_text)
            Logger.info(error_text)

    def on_save_button(self, path, file_name):
        data = export_keys()
        self.save_to_storage(path, file_name, data)

    def save_to_storage(self, path, filename, data):
        """
        Write json formatted wallet keys to storage
        """
        try:
            with open(os.path.join(path, filename), 'w+') as stream:
                stream.write(data)

            self.dismiss_popup()
        except Exception as e:
            error_text = str(e)
            self.show_info_popup(error_text)
            Logger.info(error_text)

    # end section for file chooser ######

    def validate_password(self, password):
        """
        Generate a new wallet and QRcode images if password is correct
        """
        if password == settings["confirm_delete_text"]:
            self.app_settings = Settings.get(Settings._id == 1)
            self.create_wallet(
                self.app_settings.coin_prefix,
                supported_coins[self.app_settings.coin_prefix]["testnet"]
            )
            self.generate_qr_code()
            self.confirm_popup.popup_text.text = tr._("Done!")
            self.confirm_popup.delete_button.disabled = True
            self.confirm_popup.popup_text.text = tr._("New wallet generated!")
            self.root.ids.sm.current_record.text = tr._("Recently Changed")
            Logger.debug("New wallet generated!")
        else:
            self.confirm_popup.popup_text.text = tr._("Wrong password!")
            Logger.debug("Wrong password provided!")

    # ~ def show_notification(self):
        # ~ plyer.notification.notify(
            # ~ title='QR Code generator', message="Qr code created")

    def build(self):
        self.icon = app_icon_file
        Builder.load_file(layout_file)
        if platform != 'android':
            Window.size = (360, 800)
        return MainBox()

    def on_start(self, **kwargs):
        self.first_start()


if __name__ == '__main__':
    MainApp().run()
