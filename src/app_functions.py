import os
import pyqrcode  # Generates QRcodes
import logging
from bitcoinaddress import Wallet
from ecashaddress.convert import Address
import base58
import hashlib


logger = logging.getLogger('__main__.' + __name__)


def generate_qr_code_function(string, directory, file_location):
    """Generate QR code for address and WIF
    """

    if not os.path.exists(directory):
        # if the media directory is not present
        # then create it.
        os.makedirs(directory)

    # Generating QRcode for wallet address
    qr = pyqrcode.create(string, mode='binary')

    # Saving QRcode to png file using pypng
    qr.png(file_location, scale=15)

    # Logging the operation to loglevel debug
    logger.debug(f"generate_qr_code: file {file_location} created.")


def get_wif_address(prefix, testnet=False, _wif=None):
    """Get addresss and WIF provided `prefix` and testnet
    """
    try:
        if prefix not in ("bitcoin", "bitcoin_test"):
            if not testnet:
                wallet = Wallet(_wif, testnet=False)
                wif = wallet.key.mainnet.wifc
                address = Address.from_string(
                                        wallet.address.mainnet.pubaddr1c
                                        ).to_cash_address(prefix=prefix)
                logger.debug(f"get_wif_address: Generated mainnet wallet")
            else:
                wallet = Wallet(_wif, testnet=False)
                wif = wallet.key.testnet.wifc
                address = Address.from_string(
                                        wallet.address.testnet.pubaddr1c
                                        ).to_cash_address(prefix=prefix)
                logger.debug(f"get_wif_address: Generated testnet wallet")
        else:
            if not testnet:
                wallet = Wallet(_wif, testnet=False)
                address = wallet.address.mainnet.pubaddrbc1_P2WPKH
                wif = wallet.key.mainnet.wifc
                logger.debug(f"get_wif_address: Generated Bitcoin wallet")
            else:
                wallet = Wallet(_wif, testnet=False)
                wif = wallet.key.testnet.wifc
                address = wallet.address.testnet.pubaddrtb1_P2WPKH
                logger.debug(f"get_wif_address: Generated test Bitcoin wallet")
        return {"wif": wif, "address": address}
    except Exception as e:
        logger.debug(f"get_wif_address: inside exception {e}")
        raise e


def wif_to_priv(key: str, from_compress: bool):
    """Convert WIF to private key"""
    decoded_data = base58.b58decode(key)
    if from_compress:
        raw_private_key = decoded_data[1:-5]
    else:
        raw_private_key = decoded_data[1:-4]

    return base58.b58encode(raw_private_key).decode()


def priv_to_wif(key: str, to_compress: bool):
    """Convert a private key to WIF"""
    with_prefix = b"\x80" + base58.b58decode(key)
    if to_compress:
        with_prefix = with_prefix + b"\x01"

    first_hash = hashlib.sha256(with_prefix).digest()
    second_hash = hashlib.sha256(first_hash).digest()
    checksum = second_hash[:4]

    return base58.b58encode(with_prefix + checksum).decode()


def convert_comp_wif_to_uncomp(compressed_wif):
    """Convert compressed WIF to uncompressed WIF"""
    private_key = wif_to_priv(compressed_wif, from_compress=True)
    return priv_to_wif(private_key, to_compress=False)
